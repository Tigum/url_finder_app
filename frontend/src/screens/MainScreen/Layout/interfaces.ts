export interface MainScreenLayoutProps {
    isLoading: boolean
    linksList: string[]
    getLinks: (url: string) => void
    url: string
    setUrl: (url: string) => void
    errorMsg: string
    searchStarted: boolean
    resetState: () => void
    getUrls: () => void
    showRecordsButton: boolean
    isRecords: boolean
    setIsRecords: (value: boolean) => void
    deleteUrl: (url: number) => void
    urlsList: URLTyoe[]
}

export interface URLTyoe {
    created_at: string
    id: number
    modified_at: string
    url: string
}