import React, { FunctionComponent } from 'react'
import useScreen from '../../../hooks/useScreen'
import { Form, Button, ListGroup, Spinner } from 'react-bootstrap'
import { MainScreenLayoutProps, URLTyoe } from './interfaces'

const Layout: FunctionComponent<MainScreenLayoutProps> = ({
    isLoading,
    linksList,
    getLinks,
    url,
    setUrl,
    errorMsg,
    searchStarted,
    resetState,
    getUrls,
    showRecordsButton,
    isRecords,
    setIsRecords,
    deleteUrl,
    urlsList
}) => {
    const { height, width } = useScreen()
    const { mainDiv, innerDiv } = styles
    const validateLinksListAndLength: boolean | null | undefined = linksList && linksList.length < 1
    const noResult: boolean = searchStarted && validateLinksListAndLength
    const maxWidth: number | string = width ? width * 0.9 : '90%'
    const listExists: boolean = linksList && linksList.length > 0
    const urlsExist: boolean = urlsList && urlsList.length > 0
    const showList: boolean = listExists || urlsExist

    const displayResultsNumber = (): string => {
        if (isLoading) {
            return 'Loading results...'
        }

        if (linksList) {
            return linksList.length > 0 ? linksList.length > 1 ? `${linksList.length} links found` : `${linksList.length} link found` : 'No links found'
        }
        return ''
    }

    const handleList = (): any => {
        if (isRecords) {
            return urlsList.map((url: URLTyoe, i: number) => {
                return <div style={{ display: 'flex' }} key={i}>
                    <Button variant="link" disabled={isLoading} onClick={() => { deleteUrl(url.id) }} style={{ fontSize: 14, marginRight: 5, zIndex: 100 }}>Remover</Button>
                    <ListGroup.Item onClick={() => {
                        setUrl(url.url)
                        getLinks(url.url)
                    }} style={{ cursor: 'pointer', flexWrap: 'wrap', width: maxWidth }}>
                        {url.url}
                    </ListGroup.Item>
                </div>
            })
        }
        return linksList.map((link: string, i: number) => {
            return <div style={{ display: 'flex' }} key={i}>
                <ListGroup.Item onClick={() => {
                    setUrl(link)
                    getLinks(link)
                }} style={{ cursor: 'pointer', flexWrap: 'wrap', width: maxWidth }}>
                    {link}
                </ListGroup.Item>
            </div>
        })
    }

    return (
        <div style={{ ...mainDiv }}>
            <div style={{ ...innerDiv, flexDirection: 'column', alignItems: 'center', height: showList ? 'auto' : height, width: maxWidth }}>
                {noResult === undefined
                    ?
                    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <Form.Text style={{ fontSize: 30, fontWeight: 100, marginBottom: 20 }}>No links found. Try again.</Form.Text>
                        <Button variant="primary" type="submit" onClick={resetState}>Start New Search</Button>
                    </div>
                    :

                    showList
                        ?
                        <ListGroup style={{ width: maxWidth }}>
                            <div style={{ flexDirection: 'row', display: 'flex', alignItems: 'center', marginBottom: 20, marginTop: 15 }}>
                                <Form.Text style={{ fontSize: 30, marginRight: 30, fontWeight: 100 }}> {displayResultsNumber()}</Form.Text>
                                <Button variant="primary" type="submit" onClick={resetState} style={{ height: 40, marginTop: 10 }} disabled={isLoading}>Start New Search</Button>
                            </div>
                            <div style={{ flexDirection: 'column', display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginBottom: 30 }}>
                                {url.length > 0 && <Form.Text style={{ fontSize: 15, marginRight: 30, fontWeight: 300 }}> Current URL: {isLoading ? 'Loading...' : url}</Form.Text>}
                            </div>
                            {isLoading
                                ?
                                <div style={{ width: maxWidth, display: 'flex', justifyContent: 'center', alignItems: 'center', height: height ? height * 0.8 : 'auto' }}>
                                    <Spinner animation="border" variant="primary" />
                                </div>
                                :
                                handleList()}
                        </ListGroup>
                        :
                        <Form style={{ display: 'flex', flexDirection: 'column' }} onSubmit={(event) => {
                            event?.preventDefault()
                            getLinks(url)
                        }}>
                            <Form.Text style={{ fontSize: 30, marginBottom: 20, fontWeight: 200 }}>Search for links within an URL</Form.Text>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Control type="text" placeholder="Type an URL" onChange={(event: any): void => { setUrl(event.target.value) }} value={url} />
                                <Form.Text className="text-muted">
                                    Type valid url here (Example: http://www.google.com).
                        </Form.Text>
                            </Form.Group>

                            <Button variant="primary" disabled={isLoading} onClick={() => getLinks(url)}>
                                {isLoading ? <Spinner animation="border" variant="light" /> : 'Get links from URL'}
                            </Button>
                            {showRecordsButton && <Button style={{ marginTop: 15, backgroundColor: 'transparent', color: '#037bfe' }} variant="primary" disabled={isLoading} onClick={() => {
                                setIsRecords(true)
                                getUrls()
                            }}>
                                {isLoading ? <Spinner animation="border" variant="primary" /> : 'Check previous searches'}
                            </Button>}
                            {errorMsg.length > 0 && <Form.Text style={{ color: 'red', marginTop: 10 }}>{errorMsg}</Form.Text>}
                        </Form>}
            </div>
        </div>
    )
}

const styles = {
    mainDiv: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1,
        height: '100%'
    },
    innerDiv: {
        width: '90%',
        display: 'flex',
        justifyContent: 'center',
    }
}

export default Layout