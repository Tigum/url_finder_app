import React, { FunctionComponent, useEffect } from 'react'
import useUrl from '../../hooks/useUrl'
import Layout from './Layout'

const MainScreen: FunctionComponent = () => {
    const {
        isLoading,
        linksList,
        getLinksFromUrl,
        url,
        setUrl,
        errorMsg,
        resetState,
        searchStarted,
        getUrls,
        checkIfThereAreRecords,
        showRecordsButton,
        isRecords,
        setIsRecords,
        deleteUrl,
        urlsList
    } = useUrl()

    useEffect(() => {
        let isSubscribed = true
        if (isSubscribed) {
            checkIfThereAreRecords()
        }
        return () => {
            isSubscribed = false
        }
        // eslint-disable-next-line
    }, [])

    return <Layout
        isLoading={isLoading}
        linksList={linksList}
        getLinks={(value: string) => getLinksFromUrl(value)}
        url={url}
        setUrl={(value: string) => setUrl(value)}
        errorMsg={errorMsg}
        searchStarted={searchStarted}
        resetState={resetState}
        getUrls={getUrls}
        showRecordsButton={showRecordsButton}
        isRecords={isRecords}
        setIsRecords={(value: boolean) => setIsRecords(value)}
        deleteUrl={(url_id: number) => deleteUrl(url_id)}
        urlsList={urlsList}
    />
}

export default MainScreen