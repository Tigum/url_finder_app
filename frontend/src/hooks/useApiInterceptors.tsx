import axios from 'axios'

const useApiInterceptors = () => {
    const ConfigApiRequests = () => axios.interceptors.request.use(async (config) => {

        config.headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Cache-Control': 'no-cache',
        }

        // console.log(config)
        return config
    }, err => console.log('INTERCEPTOR REQUEST ERROR ->', err))

    return {
        ConfigApiRequests
    }

}

export default useApiInterceptors