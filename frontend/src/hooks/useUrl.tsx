import { Dispatch, SetStateAction, useState } from "react"
import axios from 'axios'
import { API_URL } from '../utils/api_config'
import moment from 'moment'

interface useUrlOutput {
    isLoading: boolean
    linksList: string[]
    getLinksFromUrl: (url: string) => void
    url: string
    setUrl: Dispatch<SetStateAction<string>>
    errorMsg: string
    searchStarted: boolean
    setSearchStarted: Dispatch<SetStateAction<boolean>>
    resetState: () => void
    getUrls: () => void
    checkIfThereAreRecords: () => Promise<void>
    showRecordsButton: boolean
    isRecords: boolean
    setIsRecords: Dispatch<SetStateAction<boolean>>
    deleteUrl: (url: number) => void
    urlsList: URLTyoe[]
}

interface URLTyoe {
    created_at: string
    id: number
    modified_at: string
    url: string
}

const useUrl = (): useUrlOutput => {
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [linksList, setLinksList] = useState<string[]>([])
    const [urlsList, setUrlsList] = useState<URLTyoe[]>([])
    const [url, setUrl] = useState<string>('')
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [searchStarted, setSearchStarted] = useState<boolean>(false)
    const [showRecordsButton, setShowRecordsButton] = useState<boolean>(false)
    const [isRecords, setIsRecords] = useState<boolean>(false)

    const handleUrl = (url: string) => {
        const isHttp = url.substring(0, 7) === 'http://'
        const isHttps = url.substring(0, 8) === 'https://'
        if (!isHttp && !isHttps) {
            return `http://${url}`
        }
        return url
    }

    const getLinksFromUrl = async (url: string): Promise<void> => {
        setIsRecords(false)
        setErrorMsg('')
        if (url.length < 1) {
            setErrorMsg('Please provide url')
            return
        }

        setIsLoading(true)
        try {
            const result = await axios.post(`${API_URL}/url/`, { url: handleUrl(url.trim()) })
            setSearchStarted(true)
            setLinksList(result.data.data.links)
            setIsLoading(false)
            return
        } catch (err) {
            console.log('URL ==>', `${API_URL}/url/`)
            console.log('ERROR GETTING LINKS FROM URL ==> ', err)
            setErrorMsg('URL inválido. Tente novamente com outro URL.')
            setIsLoading(false)
            return
        }
    }

    const getUrls = async (): Promise<void> => {
        setErrorMsg('')
        setIsLoading(true)
        try {
            const result = await axios.get(`${API_URL}/url/`)
            let urls_info: URLTyoe[] = result.data.data
            urls_info.sort((a: URLTyoe, b: URLTyoe) => moment(a.created_at).format() > moment(b.created_at).format() ? -1 : 1)
            setUrlsList(urls_info)
            setIsLoading(false)
        } catch (err) {
            console.log('ERROR FETCHING URLS', err)
            setErrorMsg('Ops! An error occurred. Please try again!')
            setIsLoading(false)
            return
        }
    }

    const deleteUrl = async (url_id: number): Promise<void> => {
        setErrorMsg('')
        try {
            await axios.delete(`${API_URL}/url/?id=${url_id}`)
            await getUrls()
            await checkIfThereAreRecords()
        } catch (err) {
            setErrorMsg('There was an error trying to delete this record. Please try again!')
            console.log('ERROR DELETING URL => ', err)
            return
        }
    }

    const checkIfThereAreRecords = async (): Promise<void> => {
        try {
            const result = await axios.get(`${API_URL}/url/`)
            const urls_info: URLTyoe[] = result.data.data
            setShowRecordsButton(urls_info.length > 0)
            return
        } catch (err) {
            console.log('ERROR CHECKING IF THERE ARE RECORDS', err)
            setShowRecordsButton(false)
            return
        }
    }

    const resetState = (): void => {
        setUrlsList([])
        setIsRecords(false)
        checkIfThereAreRecords()
        setIsLoading(false)
        setLinksList([])
        setUrl('')
        setErrorMsg('')
        setSearchStarted(false)
    }

    return {
        isLoading,
        linksList,
        getLinksFromUrl,
        url,
        setUrl,
        errorMsg,
        searchStarted,
        setSearchStarted,
        resetState,
        getUrls,
        checkIfThereAreRecords,
        showRecordsButton,
        isRecords,
        setIsRecords,
        deleteUrl,
        urlsList
    }

}

export default useUrl