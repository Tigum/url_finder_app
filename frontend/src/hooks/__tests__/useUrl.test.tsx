import React from 'react'
import axios from 'axios'
import { renderHook, act } from '@testing-library/react-hooks'
import useUrl from '../useUrl'


jest.mock('axios')

describe('Testing custom hook useAddTask', () => {

    beforeEach(() => { jest.clearAllMocks() })



    it('tests getLinksFromUrl function - success', async () => {

        const url = 'http://www.facebook.com'
        const links = ['www.example.com']
        const { result } = renderHook(() => useUrl())

        await act(async () => {
            const axios_res = {
                status: 200,
                data: {
                    msg: 'success',
                    data: {
                        links,
                        url: {
                            url,
                            created_at: '2020-09-16T22:14:57.970423',
                            modified_at: '2020-09-16T22:14:57.970423',
                            id: 1
                        }
                    }
                }
            }

            //Mocking Axios response
            await axios.post.mockResolvedValue(axios_res)
            result.current.getLinksFromUrl(url)
        })

        expect(result.current.isRecords).toBe(false)
        expect(result.current.errorMsg).toBe('')
        expect(result.current.searchStarted).toBe(true)
        expect(result.current.linksList).toBe(links)
        expect(result.current.isLoading).toBe(false)
    })

    it('tests getLinksFromUrl function - fail with no url passed', async () => {

        const url = ''
        const links = ['www.example.com']
        const { result } = renderHook(() => useUrl())

        await act(async () => {
            const axios_res = {
                status: 200,
                data: {
                    msg: 'success',
                    data: {
                        links,
                        url: {
                            url,
                            created_at: '2020-09-16T22:14:57.970423',
                            modified_at: '2020-09-16T22:14:57.970423',
                            id: 1
                        }
                    }
                }
            }

            //Mocking Axios response
            await axios.post.mockResolvedValue(axios_res)
            result.current.getLinksFromUrl(url)
        })

        expect(result.current.isRecords).toBe(false)
        expect(result.current.errorMsg).toBe('Please provide url')

    })

    it('tests getUrls function', async () => {

        const urls = ['www.example.com']
        const { result } = renderHook(() => useUrl())

        await act(async () => {
            const axios_res = {
                status: 200,
                data: {
                    msg: 'success',
                    data: urls
                }
            }

            //Mocking Axios response
            await axios.get.mockResolvedValue(axios_res)
            result.current.getUrls()
        })

        expect(result.current.errorMsg).toBe('')
        expect(result.current.urlsList).toBe(urls)
    })

    it('tests checkIfThereAreRecords function', async () => {

        const urls = ['www.example.com']
        const { result } = renderHook(() => useUrl())

        await act(async () => {
            const axios_res = {
                status: 200,
                data: {
                    msg: 'success',
                    data: urls
                }
            }

            //Mocking Axios response
            await axios.get.mockResolvedValue(axios_res)
            result.current.checkIfThereAreRecords()
        })

        expect(result.current.showRecordsButton).toBe(true)
    })

    it('should set url value', async () => {
        const url: string = 'www.ibm.com'
        const { result } = renderHook(() => useUrl())

        act(() => {
            result.current.setUrl(url)
        })
        expect(result.current.url).toBe(url)
    })

    it('should set searchStarted value', async () => {
        const { result } = renderHook(() => useUrl())

        act(() => {
            result.current.setSearchStarted(true)
        })
        expect(result.current.searchStarted).toBe(true)
    })

    it('should set isRecords value', async () => {
        const { result } = renderHook(() => useUrl())

        act(() => {
            result.current.setIsRecords(true)
        })
        expect(result.current.isRecords).toBe(true)
    })
})

