import React from 'react';
import './App.css';
import useApiInterceptors from './hooks/useApiInterceptors';
import MainScreen from './screens/MainScreen';


const App = () => {
  const { ConfigApiRequests } = useApiInterceptors()

  ConfigApiRequests()

  return (
    <MainScreen />
  )
}

export default App;
