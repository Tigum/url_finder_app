echo "Building backend and postgres images..."
docker-compose -f docker-compose.prod.yml up -d --build --remove-orphans

echo "Building frontend and executing it..."
cd frontend
npm run build
docker buildv -f Dockerfile.prod -t url_finder_react .

echo ('Pushing frontend image to IBM Cloud...')
docker tag url_finder_react us.icr.io/thiagoscolari/app:v1
docker push us.icr.io/thiagoscolari/app:v1

echo ('Pushing frontend image to IBM Cloud...')
docker tag url_finder_app_api us.icr.io/thiagoscolari/api:v1
docker push us.icr.io/thiagoscolari/api:v1