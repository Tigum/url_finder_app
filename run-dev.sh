echo "Building backend and postgres images..."
docker-compose up -d --build --remove-orphans

echo "Building frontend and executing it..."
cd frontend
sudo npm install
sudo npm run build
sudo docker build -t url_finder_react .
sudo docker run -d -i -t -v ${PWD}:/app -v /app/node_modules -p 3001:5000 url_finder_react