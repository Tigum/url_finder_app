import unittest
import os
import json
from ..app import create_app, db
from ..utils.CustomResponse import CREATION_CODE, ERROR_CODE, DELETED_CODE, SUCCESS_CODE


class UrlsTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.create_url = {
            "url": "http://www.globo.com",
        }

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_url_create(self):
        """ test url record creation """
        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

    def test_url_create_fail_with_no_url(self):
        """ test url record creation - fail with no url provided"""
        self.create_url["url"] = ""
        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "Please provide url.")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_url_create_fail_with_invalid_url(self):
        """ test url record creation - fail with no invalid provided"""
        self.create_url["url"] = "http://www.globo.com1"
        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "URL Provided is invalid")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_url_delete(self):
        """ test url record deletion """
        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        res = self.client().delete(
            "/api/v1/url/?id={}".format(json_data["data"]["url"]["id"]),
            headers={"Content-Type": "application/json"},
        )
        self.assertEqual(res.status_code, DELETED_CODE)

    def test_url_delete_fail_with_no_url(self):
        """ test url record deletion - fail with no url id provided"""
        res = self.client().delete(
            "/api/v1/url/?id={}".format(""),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "URL ID not provided.")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_url_delete_fail_with_unexisting_url(self):
        """ test url record deletion - fail with unexisting url provided"""
        res = self.client().delete(
            "/api/v1/url/?id={}".format(1),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "URL does not exist.")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_urls_fetching(self):
        """ test fetching urls """
        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        res = self.client().get(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 1)
        self.assertEqual(res.status_code, SUCCESS_CODE)

    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
