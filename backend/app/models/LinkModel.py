from marshmallow import fields, Schema
import datetime
from . import db


class LinkModel(db.Model):

    __tablename__ = "links"

    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(), nullable=False)
    url = db.Column(db.Integer, db.ForeignKey("urls.id"))
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)

    def __init__(self, data):
        self.link = data.get("link")
        self.url = data.get("url")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_links_from_url_id(url_id):
        return LinkModel.query.filter_by(url=url_id).all()

    @staticmethod
    def get_link_by_link(link):
        return LinkModel.query.filter_by(link=link).first()


class LinkSchema(Schema):
    id = fields.Int(dump_only=True)
    link = fields.Str(required=True)
    url = fields.Int(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


link_schema = LinkSchema()
