from marshmallow import fields, Schema
import datetime
from . import db
from .LinkModel import LinkSchema


class UrlModel(db.Model):

    __tablename__ = "urls"

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(), nullable=False)
    links = db.relationship("LinkModel", backref="links_url", cascade="all, delete")
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)

    def __init__(self, data):
        self.url = data.get("url")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        ser_url = url_schema.dump(self)
        for key in data:
            if ser_url[key] != data[key]:
                setattr(self, key, data[key])
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_urls():
        return UrlModel.query.all()

    @staticmethod
    def get_one_url(id):
        return UrlModel.query.get(id)

    @staticmethod
    def get_url_by_url(url):
        return UrlModel.query.filter_by(url=url).first()


class UrlSchema(Schema):
    id = fields.Int(dump_only=True)
    url = fields.Str(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


url_schema = UrlSchema()
