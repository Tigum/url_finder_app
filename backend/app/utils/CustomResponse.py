from flask import json, Response

SUCCESS_CODE = 200
CREATION_CODE = 201
DELETED_CODE = 204
ERROR_CODE = 400


def custom_response(msg, res, status_code):
    return Response(
        mimetype="application/json",
        response=json.dumps({"msg": msg, "data": res}),
        status=status_code,
    )
