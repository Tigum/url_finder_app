from flask import request, json, Response, Blueprint, g
from ..models.LinkModel import LinkModel, link_schema
from ..models.UrlModel import UrlModel, url_schema
from ..utils.CustomResponse import (
    SUCCESS_CODE,
    ERROR_CODE,
    custom_response,
)

link_api = Blueprint("link_api", __name__)


@link_api.route("/", methods=["GET"])
def get_links():
    url = request.args.get("url")

    if not url:
        return custom_response("URL not provided.", [], ERROR_CODE)

    url_exists = UrlModel.get_url_by_url(url)
    if url_exists:
        url_info = url_schema.dump(url_exists)
        url_id = url_info["id"]
        fetched_links = LinkModel.get_all_links_from_url_id(url_id)
        links = []
        for fetched_link in fetched_links:
            link = link_schema.dump(fetched_link)
            links.append(link)
        return custom_response("success", links, SUCCESS_CODE)
    return custom_response("URL does not exist in the DB.", [], ERROR_CODE)
