#!/bin/bash


echo "Creating the volume..."

kubectl apply -f ./kubernetes/persistent-volume.yml
kubectl apply -f ./kubernetes/persistent-volume-claim.yml


echo "Creating the postgres deployment and service..."

kubectl apply -f ./kubernetes/postgres-deploy.yml
kubectl apply -f ./kubernetes/postgres-service.yml



echo "Creating the api deployment and service..."

kubectl apply -f ./kubernetes/api-deploy.yml
kubectl apply -f ./kubernetes/api-service.yml



echo "Creating the app deployment and service..."

kubectl apply -f ./kubernetes/app-deploy.yml
kubectl apply -f ./kubernetes/app-service.yml