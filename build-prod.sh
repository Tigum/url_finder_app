echo "Build API and Database..."
docker-compose -f docker-compose.prod up -d --build

echo "Building frontend application..."
docker build -t url_finder ./frontend